import React from 'react';

export default class ClassTest extends React.Component {
    render() {
        const {test} = this.props;

        return (
            <p>I am class based component. {test} </p>
        )
    }
}