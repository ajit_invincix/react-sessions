import logo from './logo.svg';
import './App.css';
import ClassTest from './ClassTestComponent';
import {useEffect, useState} from 'react';

export function App() {
  const [headerValue, setHeaderValue] = useState("Hi")
   console.log("App Component")

  // var headerValue = "Hi";

  console.log("state ", useState("hi"))
  useEffect(() => {
  }, []);


  // function excuteState() {
  //   headerValue="hello"; 
  //   console.log("clicked");
  // }


  return (
    <div>
      <h1>{headerValue} React </h1>
      
      <button onClick={() => {setHeaderValue(Math.random() * 1000)}}>Click Me</button>
    </div>
  );
}



















// export function Test() {
//   return (
//     <p>I am another component</p>
//   )
// };


// export function Test2() {
//   return (
//     <p>I am another component 2</p>
//   )
// };